package z.somniatis.routify.routifyServer.annotations.processors;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import z.routify.routifymodel.shared.models.User;
import z.routify.routifymodel.shared.models.user.Role;
import z.somniatis.routify.routifyServer.annotations.AnnotationProcessor;
import z.somniatis.routify.routifyServer.annotations.Logical;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.api.AuthorizationSP;
import z.somniatis.routify.routifyServer.api.context.RequestContext;
import z.somniatis.routify.routifyServer.api.errors.AuthSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;

import javax.ws.rs.core.HttpHeaders;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: Zun
 * Date: 14.1.2014
 * Time: 17:19
 */
public class RequiresPermissionsProcessor implements AnnotationProcessor<RequiresPermissions> {
    @Inject private Jongo jongo;
    @Inject @Named("SECRET_KEY") private String SECRET_KEY;
    @Inject private RequestContext requestContext;

    @Override
    public RequestResult process(RequiresPermissions requiresPermissions, Method method, HttpHeaders headers) {
        RequestResult requestResult;

        try {
            String authorization = headers.getHeaderString("Authorization");
            if (Strings.isNullOrEmpty(authorization) || !authorization.startsWith(AuthorizationSP.BEARER)){
                requestResult = new RequestResult(AuthSpError.AUTHENTICATION_IS_REQUIRED);
            } else {
                String token = authorization.replace(AuthorizationSP.BEARER, "");
                SignedJWT signedJWT = SignedJWT.parse(token);

                JWSVerifier verifier = new MACVerifier(SECRET_KEY);

                boolean verify = signedJWT.verify(verifier);
                if (!verify){
                    requestResult = new RequestResult(AuthSpError.AUTHENTICATION_IS_REQUIRED);
                } else {
                    ReadOnlyJWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();

                    // check if this token is valid token of the user
                    String userId = claimsSet.getSubject();
                    MongoCollection userDb = jongo.getCollection("user");
                    User user = userDb.findOne("{_id: #}", new ObjectId(userId)).projection("{password : 0  }").as(User.class);
                    if (user == null) {
                        requestResult = new RequestResult(AuthSpError.AUTHENTICATION_IS_REQUIRED);
                    } else {
                        // check token part, if tokenPart is match with current token then it is valid token
                        // otherwise it is invalid due to the user revoked or refreshed her token
                        String tokenPart = user.getTokenPart();
                        if (Strings.isNullOrEmpty(tokenPart) || !token.endsWith(tokenPart)) {
                            requestResult = new RequestResult(AuthSpError.AUTHENTICATION_IS_REQUIRED);
                        } else {
                            MongoCollection roleDb = jongo.getCollection("role");
                            MongoCollection permissionDb = jongo.getCollection("permission");

                            String[] permissionNames = requiresPermissions.value();
                            List<ObjectId> roleIds = user.getRoleIds();
                            Iterator<Role> roleIterator = roleDb.find("{_id: {$in: #}}", roleIds).projection("{permissionIds: 1}").as(Role.class).iterator();
                            ArrayList<Role> roles = Lists.newArrayList(roleIterator);

                            List<ObjectId> permissionIds = new ArrayList<ObjectId>();
                            for(Role role : roles){
                                permissionIds.addAll(role.getPermissionIds());
                            }

                            ArrayList<String> permissionNameList = Lists.newArrayList(permissionNames);
                            permissionNameList.add("AllPermission");
                            List<String> permissions = permissionDb.distinct("name").query("{_id: {$in: #}, name: {$in: #}}", permissionIds, permissionNameList).as(String.class);

                            // check AllPermission permission
                            if (permissions.contains("AllPermission")){
                                requestResult = new RequestResult(true);
                            } else {
                                if (requiresPermissions.logical().equals(Logical.AND)){
                                    if (permissions.size() == permissionNames.length){
                                        requestResult = new RequestResult(true);
                                    } else {
                                        requestResult = new RequestResult(AuthSpError.PERMISSION_IS_REQUIRED);
                                    }
                                } else {
                                    if (permissions.size() > 0){
                                        requestResult = new RequestResult(true);
                                    } else {
                                        requestResult = new RequestResult(AuthSpError.PERMISSION_IS_REQUIRED);
                                    }
                                }
                            }

                            // set requestcontext
                            if (requestResult.getResult()){
                                requestContext.setUser(user);
                            }
                        }
                    }
                }
            }
        } catch (ParseException e) {
            requestResult = new RequestResult<String>(AuthSpError.AUTHENTICATION_IS_REQUIRED);
        } catch (JOSEException e) {
            requestResult = new RequestResult<String>(AuthSpError.AUTHENTICATION_IS_REQUIRED);
        } catch (Exception e) {
            e.printStackTrace();
            requestResult = new RequestResult<String>(AuthSpError.UNKNOWN_SERVER_ERROR_WHEN_AUTHORIZING);
        }

        return requestResult;
    }
}