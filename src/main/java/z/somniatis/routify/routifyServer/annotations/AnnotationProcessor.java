package z.somniatis.routify.routifyServer.annotations;

import z.somniatis.routify.routifyServer.api.errors.RequestResult;

import javax.ws.rs.core.HttpHeaders;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * User: Zun
 * Date: 14.1.2014
 * Time: 17:10
 */
public interface AnnotationProcessor<T extends Annotation> {
    RequestResult process(T annotation, Method method, HttpHeaders httpHeaders);
}


