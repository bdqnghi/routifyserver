package z.somniatis.routify.routifyServer.guiceModules;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import z.somniatis.routify.routifyServer.annotations.AnnotationProcessor;
import z.somniatis.routify.routifyServer.annotations.RequiresAuthentication;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.annotations.RequiresRoles;
import z.somniatis.routify.routifyServer.annotations.processors.RequiresAuthenticationProcessor;
import z.somniatis.routify.routifyServer.annotations.processors.RequiresPermissionsProcessor;
import z.somniatis.routify.routifyServer.annotations.processors.RequiresRolesProcessor;
import z.somniatis.routify.routifyServer.api.context.RequestContext;
import z.somniatis.routify.routifyServer.api.utils.DistrictGeoJsonUtil;
import z.somniatis.routify.routifyServer.background.BackgroundService;
import z.somniatis.routify.routifyServer.background.BackgroundServiceManager;

import java.lang.annotation.Annotation;

/**
 * User: Zun
 * Date: 6/7/14
 * Time: 11:41 AM
 */
public class RoutifyServerModule extends AbstractModule {
    @Override
    protected void configure() {
        //binding background services
        bind(BackgroundServiceManager.class).in(Singleton.class);
        Multibinder<BackgroundService> backgroundServices = Multibinder.newSetBinder(binder(), BackgroundService.class);

        // binding annotation processors
        MapBinder<Class<? extends Annotation>, AnnotationProcessor> processorMapBinder =MapBinder.newMapBinder(
                binder(),
                new TypeLiteral<Class<? extends Annotation>>(){},
                new TypeLiteral<AnnotationProcessor>(){},
                Names.named("AnnotationProcessors"));
        processorMapBinder.addBinding(RequiresAuthentication.class).to(RequiresAuthenticationProcessor.class).in(Singleton.class);
        processorMapBinder.addBinding(RequiresPermissions.class).to(RequiresPermissionsProcessor.class).in(Singleton.class);
        processorMapBinder.addBinding(RequiresRoles.class).to(RequiresRolesProcessor.class).in(Singleton.class);

        bind(RequestContext.class).in(Singleton.class);
        bind(DistrictGeoJsonUtil.class).in(Singleton.class);
        bindConstant().annotatedWith(Names.named("SECRET_KEY")).to("8g*$#I9v|A-)8a+Yd1..;uBA*7EV0bR(|Z?%6Bo:A)l^+@!ab/Ey]_>3O{lso/MZ");
    }
}
