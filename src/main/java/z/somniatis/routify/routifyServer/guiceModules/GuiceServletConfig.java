package z.somniatis.routify.routifyServer.guiceModules;

import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.servlet.*;
import z.somniatis.routify.routifyServer.background.BackgroundServiceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Zun
 * Date: 6/7/14
 * Time: 11:46 AM
 */

public class GuiceServletConfig extends GuiceServletContextListener {
    public static Injector INJECTOR;

    @Override
    public Injector getInjector() {
        List<Module> guiceModules = new ArrayList<Module>();
        guiceModules.add(new RoutifyServerModule());
        guiceModules.add(new DbModule());
        guiceModules.add(new RoutifyServletModule());
        InjectorProvider.init(guiceModules);

        Injector injector = InjectorProvider.getInjector();
        BackgroundServiceManager backgroundServiceManager = injector.getInstance(BackgroundServiceManager.class);
        backgroundServiceManager.start();

        GuiceServletConfig.INJECTOR = injector;
        return injector;
    }
}
