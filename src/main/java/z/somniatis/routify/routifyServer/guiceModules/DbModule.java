package z.somniatis.routify.routifyServer.guiceModules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.mongodb.*;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import z.routify.routifymodel.shared.models.Hotel;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.routify.routifymodel.shared.models.User;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Zun
 * Date: 6/7/14
 * Time: 11:23 PM
 */
public class DbModule extends AbstractModule {
    @Override
    protected void configure() {
        try {
            // morphia
            ServerAddress addr = new ServerAddress("localhost", 27017);
            List<MongoCredential> credentials = new ArrayList<MongoCredential>();
            MongoCredential routifyDbCredential = MongoCredential.createMongoCRCredential("routifyadmin", "RoutifyDb", "d0c0rr3ctly".toCharArray());
            credentials.add(routifyDbCredential);

            Mongo localhost = new MongoClient(addr, credentials);
            Morphia morphia = new Morphia();
            Datastore routifyUserDb = morphia.createDatastore(localhost, "RoutifyDb");

            morphia.map(User.class);
            morphia.map(Hotel.class);
            morphia.map(PlaceDetail.class);

            bind(Datastore.class).annotatedWith(Names.named("RoutifyDb")).toInstance(routifyUserDb);
            bind(Morphia.class).toInstance(morphia);

            // jongo
            DB db = new MongoClient(addr, credentials).getDB("RoutifyDb");
            Jongo jongo = new Jongo(db);
            bind(Jongo.class).toInstance(jongo);
        } catch (UnknownHostException e) {
            throw new Error(e);
        }
    }
}
