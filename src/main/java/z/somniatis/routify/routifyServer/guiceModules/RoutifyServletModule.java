package z.somniatis.routify.routifyServer.guiceModules;

import com.google.inject.servlet.ServletModule;
import z.somniatis.routify.routifyServer.servlets.S3UploadDelete;
import z.somniatis.routify.routifyServer.servlets.S3UploadSignature;
import z.somniatis.routify.routifyServer.servlets.S3UploadSuccess;

/**
 * User: Zun
 * Date: 6/7/14
 * Time: 11:41 AM
 */
public class RoutifyServletModule extends ServletModule {

    @Override protected void configureServlets() {
        serve("/s3/signature*").with(S3UploadSignature.class);
        serve("/s3/success*").with(S3UploadSuccess.class);
        serve("/s3/delete*").with(S3UploadDelete.class);
    }
}
