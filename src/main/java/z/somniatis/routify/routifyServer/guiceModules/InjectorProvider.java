package z.somniatis.routify.routifyServer.guiceModules;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

import java.util.List;

public class InjectorProvider {
    private static Injector injector;

    public InjectorProvider() {
    }

    public static Injector getInjector() {
        if (injector == null) {
            throw new Error("InjectorProvider should be initialized befored being used.");
        }
        return injector;
    }

    public static void init(List<Module> guiceModules) {
        injector = Guice.createInjector(guiceModules);
    }
}