package z.somniatis.routify.routifyServer.background;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

/**
 * User: Zun
 * Date: 6/5/13
 * Time: 10:30 AM
 */
public class BackgroundServiceManager {
    private static final Logger logger = LoggerFactory.getLogger(BackgroundServiceManager.class);

    public final Set<BackgroundService> backgroundServices;

    @Inject
    public BackgroundServiceManager(Set<BackgroundService> backgroundServices) {
        this.backgroundServices = backgroundServices;
    }

    public void start(){
        if (backgroundServices != null){
            for(BackgroundService backgroundService : backgroundServices){
                new Thread(backgroundService).start();
            }
        } else {
            logger.warn("BackgroundServices is null.");
        }
    }
}
