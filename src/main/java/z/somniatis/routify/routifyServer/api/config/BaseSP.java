package z.somniatis.routify.routifyServer.api.config;


import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;

/**
 * User: zWork
 * Date: 8.5.2014
 * Time: 13:27
 */
public abstract class BaseSP {
    protected BaseSP() {
        GuiceServletConfig.INJECTOR.injectMembers(this);
    }
}
