package z.somniatis.routify.routifyServer.api.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Zun
 * Date: 8/10/2014
 * Time: 11:18 AM
 */
public class DistrictGeoJsonUtil {
    private Map<String, String> geoJsonMap = new HashMap<String, String>();

    public DistrictGeoJsonUtil() {
        InputStream inputStream =
                getClass().getClassLoader().getResourceAsStream("../../resources/provinceGeoJson.json");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream ));
        String json = null;
        try {
            json = reader.readLine();
        } catch (IOException e) {
            throw new Error(e);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ArrayList<DistrictGeo> districtGeos = objectMapper.readValue(json, new TypeReference<ArrayList<DistrictGeo>>() {});
            for(DistrictGeo districtGeo : districtGeos){
                GeoJson geoJson = districtGeo.geoJson;
                geoJsonMap.put(districtGeo.name, objectMapper.writeValueAsString(geoJson));
            }
        } catch (IOException e) {
            throw new Error("json deserializing fails", e);
        }
    }

    public String getDictrictShape(String districtKey) {
        return geoJsonMap.get(districtKey);
    }

    public static class DistrictGeo {
        public String name;
        public GeoJson geoJson;

    }
    public static class GeoJson {
        public String type;
        public ArrayList<ArrayList<ArrayList<Double>>> coordinates;
    }
}
