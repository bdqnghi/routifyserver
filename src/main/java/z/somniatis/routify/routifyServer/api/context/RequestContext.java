package z.somniatis.routify.routifyServer.api.context;

import com.google.inject.Inject;
import z.routify.routifymodel.shared.models.User;

/**
 * User: Zun
 * Date: 4/11/13
 * Time: 3:48 PM
 */
public class RequestContext {
    private final static ThreadLocal<User> user = new ThreadLocal<User>();

    @Inject
    public RequestContext() {
    }

    public void setUser(User user) {
        if (user != null) {
            RequestContext.user.set(user);
        } else {
            RequestContext.user.set(null);
        }
    }

    public User getUser() {
        return user.get();
    }
}