package z.somniatis.routify.routifyServer.api.errors;

/**
 * User: Zun
 * Date: 8/4/2014
 * Time: 10:15 PM
 */
public class UserSpError {
    public final static ErrorCode AUTHEN_DATA_INCORRECT = new ErrorCode(300, "Email or password is not correct.");
}
