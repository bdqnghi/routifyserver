package z.somniatis.routify.routifyServer.api.errors;

/**
 * User: Zun
 * Date: 8/4/2014
 * Time: 10:15 PM
 */
public class AdminSpError {
    public final static ErrorCode PERMISSION_NAME_IN_USED = new ErrorCode(400, "Permission name is in used.");
    public final static ErrorCode PERMISSION_NOT_EXIST = new ErrorCode(401, "Permission does not exist.");
    public final static ErrorCode ROLE_NAME_IN_USED = new ErrorCode(402, "Role name is in used.");
    public final static ErrorCode ROLE_NOT_EXIST = new ErrorCode(403, "Role does not exist.");
    public final static ErrorCode SOME_OF_ROLES_NOT_EXIST = new ErrorCode(403, "Some of roles do not exist.");

    public final static ErrorCode USER_EMAIL_WAS_USED = new ErrorCode(404, "Email was in used.");
    public final static ErrorCode USER_PASSWORD_TOO_SHORT = new ErrorCode(405, "Password is too short.");
    public final static ErrorCode EMPTY_SUBMITTED_USER = new ErrorCode(406, "Empty submitted user");
    public final static ErrorCode USER_DATA_MISSING = new ErrorCode(407, "Some user data is missing (firstname, lastname, password, email)");
    public final static ErrorCode USER_NOT_EXIST = new ErrorCode(407, "User does not exist.");

}
