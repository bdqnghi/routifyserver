package z.somniatis.routify.routifyServer.api.errors;

/**
 * User: Zun
 * Date: 8/4/2014
 * Time: 10:15 PM
 */
public class PlaceSpError {
    public final static ErrorCode TOO_MANY_PLACES = new ErrorCode(200, "Too many places.");
    public final static ErrorCode PLACE_ID_DOES_NOT_EXIST = new ErrorCode(201, "Place id which is requested does not exist.");
}
