package z.somniatis.routify.routifyServer.api;

import com.google.common.base.Strings;
import com.google.common.hash.Hashing;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.servlet.RequestScoped;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import z.routify.routifymodel.shared.models.User;
import z.somniatis.routify.routifyServer.annotations.RequiresAuthentication;
import z.somniatis.routify.routifyServer.api.config.BaseSP;
import z.somniatis.routify.routifyServer.api.context.RequestContext;
import z.somniatis.routify.routifyServer.api.errors.AuthSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Date;

@RequestScoped
@Path("auth")
public class AuthorizationSP extends BaseSP {
    public static final String BEARER = "Bearer ";
    public static final int TOKEN_PART_LENGTH = 10;
    private final MongoCollection userDb;
    private final MACSigner signer;

    @Inject private RequestContext requestContext;
    @Inject private Jongo jongo;
    @Inject @Named("SECRET_KEY") private String SECRET_KEY;

    @Inject
    public AuthorizationSP() {
        userDb = jongo.getCollection("user");

        // Generate random 32-bit shared secret
        SecureRandom random = new SecureRandom();
        byte[] sharedSecret = new byte[32];
        random.nextBytes(sharedSecret);

        // Create HMAC signer
        signer = new MACSigner(SECRET_KEY);
    }

    @GET
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(
            @QueryParam("email") String email,
            @QueryParam("password") String password,
            @QueryParam("remember") boolean remember
    ) throws Exception {
        RequestResult<String> requestResult;
        if (Strings.isNullOrEmpty(email) || Strings.isNullOrEmpty(password)){
            requestResult = new RequestResult<String>(AuthSpError.PASSWORD_N_EMAIL_REQUIRED);
        } else {
            String hashedPassword = Hashing.md5().hashString(password, StandardCharsets.UTF_8).toString();
            User user = userDb.findOne("{emailAddress : #, password : #}", email, hashedPassword).projection("{_id: 1}").as(User.class);
            if (user == null){
                requestResult = new RequestResult<String>(AuthSpError.PASSWORD_N_EMAIL_INCORRECT);
            } else {
                String userId = user.getId().toString();
                // create token
                String token = createNewToken(remember, userId);
                requestResult = new RequestResult<String>(token);

                // save token part to user
                String latestTokenPart = token.substring(token.length() - TOKEN_PART_LENGTH);
                userDb.update("{_id: #}", user.getId()).with("{$set: {tokenPart: #}}", latestTokenPart);
            }
        }
        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/refresh")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    public Response refreshToken(@Context HttpHeaders headers) throws Exception {
        RequestResult<String> requestResult;
        String authorization = headers.getHeaderString("Authorization");
        if (!authorization.startsWith(BEARER)){
            requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
        } else {
            String token = authorization.replace(BEARER, "");
            SignedJWT signedJWT = SignedJWT.parse(token);

            JWSVerifier verifier = new MACVerifier(SECRET_KEY);

            boolean verify = signedJWT.verify(verifier);
            if (!verify){
                requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
            } else {
                ReadOnlyJWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();

                // check if this token is valid token of the user
                String userId = claimsSet.getSubject();
                User user = userDb.findOne("{_id: #}", new ObjectId(userId)).as(User.class);
                if (user == null){
                    requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
                } else {
                    // check token part, if tokenPart is match with current token then it is valid token
                    // otherwise it is invalid due to the user revoked or refreshed her token
                    String tokenPart = user.getTokenPart();
                    if (Strings.isNullOrEmpty(tokenPart) || !token.endsWith(tokenPart)) {
                        requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
                    } else {
                        long expirationTime = claimsSet.getExpirationTime().getTime();
                        long diff = expirationTime - new Date().getTime();
                        if (diff > 0){
                            requestResult = new RequestResult<String>(token);
                        } else {
                            // if token is refreshed within 15min then renew token in next 60min
                            // otherwise, force user to re-login
                            if (diff < 15* 60 * 1000){
                                // create new token
                                String newToken = createNewToken(false, userId);
                                requestResult = new RequestResult<String>(newToken);

                                // save token part to user
                                String latestTokenPart = newToken.substring(token.length() - TOKEN_PART_LENGTH);
                                userDb.update("{_id: #}", user.getId()).with("{$set: {tokenPart: #}}", latestTokenPart);
                            } else {
                                requestResult = new RequestResult<String>(AuthSpError.TOKEN_WAS_EXPIRED);
                            }
                        }
                    }
                }
            }
        }

        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/revoke")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    public Response revokeToken(@Context HttpHeaders headers) throws Exception {
        RequestResult<String> requestResult;
        String authorization = headers.getHeaderString("Authorization");
        if (!authorization.startsWith(BEARER)){
            requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
        } else {
            String token = authorization.replace(BEARER, "");
            SignedJWT signedJWT = SignedJWT.parse(token);

            JWSVerifier verifier = new MACVerifier(SECRET_KEY);

            boolean verify = signedJWT.verify(verifier);
            if (!verify){
                requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
            } else {
                ReadOnlyJWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();

                // check if this token is valid token of the user
                String userId = claimsSet.getSubject();
                User user = userDb.findOne("{_id: #}", new ObjectId(userId)).as(User.class);
                if (user == null) {
                    requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
                } else {
                    // check token part, if tokenPart is match with current token then it is valid token
                    // otherwise it is invalid due to the user revoked or refreshed her token
                    String tokenPart = user.getTokenPart();
                    if (Strings.isNullOrEmpty(tokenPart) || !token.endsWith(tokenPart)) {
                        requestResult = new RequestResult<String>(AuthSpError.REQUEST_WITH_INVALID_TOKEN);
                    } else {
                        // remove token part from user to revoke the user token completely
                        userDb.update("{_id: #}", user.getId()).with("{$set: {tokenPart: ''}}");
                        requestResult = new RequestResult<String>();
                        requestResult.setResult(true);
                    }
                }
            }
        }

        return Response.ok(requestResult).build();
    }

    private String createNewToken(boolean remember, String userId) throws JOSEException {
        // Prepare JWT with claims set
        JWTClaimsSet claimsSet = new JWTClaimsSet();
        claimsSet.setSubject(userId);

        if (remember){
            // Set expiration in 7 days
            claimsSet.setExpirationTime(new Date(new Date().getTime() + 1000*60*60*24*7));
        } else {
            // Set expiration in 60 minutes
            claimsSet.setExpirationTime(new Date(new Date().getTime() + 1000*60*60));
        }

        claimsSet.setNotBeforeTime(new Date());
        claimsSet.setIssueTime(new Date());

        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

        // Apply the HMAC
        signedJWT.sign(signer);

        // create token
        return signedJWT.serialize();
    }
}