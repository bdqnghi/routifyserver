package z.somniatis.routify.routifyServer.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.routify.routifymodel.shared.models.PlaceEditHistory;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.api.config.BaseSP;
import z.somniatis.routify.routifyServer.api.context.RequestContext;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// http://127.0.0.1:8888/api/User/
@RequestScoped
@Path("statistic")
public class EditorStatisticSP extends BaseSP {
    @Inject private Jongo jongo;
    @Inject private RequestContext requestContext;

    private final MongoCollection placeDb;
    private final MongoCollection placeHistoryDb;

    public EditorStatisticSP() {
        placeDb = jongo.getCollection("placedetail");
        placeHistoryDb = jongo.getCollection("PlaceEditHistory");
    }

    @GET
    @Path("/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Place:Edit")
    public Response getUserEditStatistic(@PathParam("email") String emailAddress){
        Iterator<PlaceEditHistory> historyIterator = placeHistoryDb.find("{$or: [{createdUserEmail: #},{'historyItems.email' : #}]}", emailAddress, emailAddress).as(PlaceEditHistory.class).iterator();
        ArrayList<PlaceEditHistory> placeEditHistories = Lists.newArrayList(historyIterator);

        List<ObjectId> placeIds = Lists.transform(placeEditHistories, new Function<PlaceEditHistory, ObjectId>() {
            @Override
            public ObjectId apply(PlaceEditHistory placeEditHistory) {
                return placeEditHistory.getPlaceId();
            }
        });

        Iterator<PlaceDetail> iterator = placeDb.find("{_id: {$in: #}}", placeIds).projection("{name : 1, disable: 1}").as(PlaceDetail.class).iterator();
        ArrayList<PlaceDetail> allPlaces = Lists.newArrayList(iterator);

        iterator = placeDb.find("{_id: {$in: #}, " +
                "name: {$exists: true, $ne: null, $ne: ''}, " +
                "description: {$exists: true, $ne: null, $ne: ''}," +
                "address: {$exists: true, $ne: null, $ne: ''}," +
                "location.lat: {$exists: true}," +
                "photos.1: {$exists: true}}", placeIds).projection("{_id: 1}").as(PlaceDetail.class).iterator();

        ArrayList<PlaceDetail> validPlaces = Lists.newArrayList(iterator);
        List<String> validPlaceIds = Lists.transform(validPlaces, new Function<PlaceDetail, String>() {
            @Override
            public String apply(PlaceDetail placeEditHistory) {
                return placeEditHistory.getId().toString();
            }
        });

        ArrayList result = new ArrayList();
        result.add(placeEditHistories);
        result.add(allPlaces);
        result.add(validPlaceIds);

        RequestResult<List> requestResult = new RequestResult<List>(result);
        return Response.ok(requestResult).build();
    }

}