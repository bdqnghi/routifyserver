package z.somniatis.routify.routifyServer.api.config.filters;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.glassfish.jersey.server.ExtendedUriInfo;
import org.glassfish.jersey.server.model.ResourceMethod;
import z.somniatis.routify.routifyServer.annotations.AnnotationProcessor;
import z.somniatis.routify.routifyServer.api.errors.AuthSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;

@Provider
public class ContextBindingFilter implements ContainerRequestFilter {
    @Context private HttpHeaders httpHeaders;
    @Inject @Named("AnnotationProcessors") Map<Class<? extends Annotation>, AnnotationProcessor> annotationHandlerMap;

    public ContextBindingFilter() {
        GuiceServletConfig.INJECTOR.injectMembers(this);
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // process annotations
        ResourceMethod matchedResourceMethod = ((ExtendedUriInfo) requestContext.getUriInfo()).getMatchedResourceMethod();
        Method handlingMethod = matchedResourceMethod.getInvocable().getHandlingMethod();
        RequestResult processResult = processAnnotations(handlingMethod);
        if (!processResult.getResult()) {
            Response response = Response.ok(processResult).build();
            throw new WebApplicationException(response);
        }
    }

    private RequestResult processAnnotations(Method method) {
        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
        for(Annotation annotation : declaredAnnotations){
            Class<? extends Annotation> annotationClass = annotation.annotationType();
            if (annotationHandlerMap.containsKey(annotationClass)){
                AnnotationProcessor annotationProcessor = annotationHandlerMap.get(annotationClass);
                RequestResult processResult = annotationProcessor.process(annotation, method, httpHeaders);
                if (!processResult.getResult()){
                    return processResult;
                }
            }
        }

        return new RequestResult(true);
    }
}