package z.somniatis.routify.routifyServer.api;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;
import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import z.routify.routifymodel.shared.models.User;
import z.routify.routifymodel.shared.models.user.Permission;
import z.routify.routifymodel.shared.models.user.Role;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.api.config.BaseSP;
import z.somniatis.routify.routifyServer.api.errors.AdminSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * User: Zun
 * Date: 9/13/2014
 * Time: 12:27 AM
 */
@RequestScoped
@Path("admin")
public class AdminSP extends BaseSP {

    private final MongoCollection permissionDb;
    private final MongoCollection roleDb;
    private final MongoCollection userDb;
    @Inject private Jongo jongo;

    @Inject
    public AdminSP() {
        permissionDb = jongo.getCollection("permission");
        roleDb = jongo.getCollection("role");
        userDb = jongo.getCollection("user");
    }

    @POST
    @Path("/permission")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response addPermission(Permission permission) throws Exception
    {
        RequestResult<Permission> requestResult;
        Permission result = permissionDb.findOne("{name : #}", permission.getName()).as(Permission.class);
        if (result != null){
            requestResult = new RequestResult<Permission>(AdminSpError.PERMISSION_NAME_IN_USED);
        } else {
            permission.setId(null);
            permissionDb.save(permission);
            requestResult = new RequestResult<Permission>(permission);
        }

        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/permission/{permissionId}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response getPermission(@PathParam("permissionId") String permissionId) throws Exception
    {
        RequestResult<Permission> requestResult;
        Permission result = permissionDb.findOne(new ObjectId(permissionId)).as(Permission.class);
        requestResult = new RequestResult<Permission>(result);
        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/permission/all")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Read")
    public Response getAllPermissions() throws Exception
    {
        Iterator<Permission> iterator = permissionDb.find().as(Permission.class).iterator();
        ArrayList<Permission> permissions = Lists.newArrayList(iterator);
        RequestResult<List<Permission>> requestResult = new RequestResult<List<Permission>>(permissions);
        return Response.ok(requestResult).build();
    }

    @PUT
    @Path("/permission/{permissionId}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response updatePermission(@PathParam("permissionId") String permissionId, Permission permission) throws Exception
    {
        RequestResult<Permission> requestResult;
        Permission result = permissionDb.findOne(new ObjectId(permissionId)).as(Permission.class);
        if (result == null){
            requestResult = new RequestResult<Permission>(AdminSpError.PERMISSION_NOT_EXIST);
        } else {
            permission.setId(new ObjectId(permissionId));
            permissionDb.save(permission);
            requestResult = new RequestResult<Permission>(permission);
        }
        return Response.ok(requestResult).build();
    }

    @DELETE
    @Path("/permission/{permissionId}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response deletePermission(@PathParam("permissionId") String permissionId) throws Exception
    {
        RequestResult<Permission> requestResult;
        Permission result = permissionDb.findOne(new ObjectId(permissionId)).as(Permission.class);
        if (result == null){
            requestResult = new RequestResult<Permission>(AdminSpError.PERMISSION_NOT_EXIST);
        } else {
            permissionDb.remove(new ObjectId(permissionId));
            requestResult = new RequestResult<Permission>((Permission)null);
        }
        return Response.ok(requestResult).build();
    }

    @POST
    @Path("/role")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response addRole(Role role) throws Exception
    {
        RequestResult<Role> requestResult;
        Role result = roleDb.findOne("{name : #}", role.getName()).as(Role.class);
        if (result != null){
            requestResult = new RequestResult<Role>(AdminSpError.PERMISSION_NAME_IN_USED);
        } else {
            roleDb.save(role);
            requestResult = new RequestResult<Role>(role);

        }
        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/role/{roleId}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Read")
    public Response getRole(@PathParam("roleId") String roleId) throws Exception
    {
        RequestResult<Role> requestResult;
        Role result = roleDb.findOne(new ObjectId(roleId)).as(Role.class);
        requestResult = new RequestResult<Role>(result);
        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/role/all")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Read")
    public Response getAllRoles() throws Exception
    {
        Iterator<Role> iterator = roleDb.find().as(Role.class).iterator();
        ArrayList<Role> roles = Lists.newArrayList(iterator);
        RequestResult<List<Role>> requestResult = new RequestResult<List<Role>>(roles);
        return Response.ok(requestResult).build();
    }

    @PUT
    @Path("/role/{roleId}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response updateRole(@PathParam("roleId") String roleId, Role role) throws Exception
    {
        RequestResult<Role> requestResult;
        Role result = roleDb.findOne(new ObjectId(roleId)).as(Role.class);
        if (result == null){
            requestResult = new RequestResult<Role>(AdminSpError.PERMISSION_NOT_EXIST);
        } else {
            role.setId(new ObjectId(roleId));
            roleDb.save(role);
            requestResult = new RequestResult<Role>(role);
        }
        return Response.ok(requestResult).build();
    }

    @DELETE
    @Path("/role/{roleId}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Authorization:Edit")
    public Response deleteRole(@PathParam("roleId") String roleId) throws Exception
    {
        RequestResult<Role> requestResult;
        Role result = roleDb.findOne(new ObjectId(roleId)).as(Role.class);
        if (result == null){
            requestResult = new RequestResult<Role>(AdminSpError.PERMISSION_NOT_EXIST);
        } else {
            roleDb.remove(new ObjectId(roleId));
            requestResult = new RequestResult<Role>((Role)null);
        }
        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/user/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("User:Read")
    public Response getUser(@PathParam("userId") String userId) throws Exception {
        User user = userDb.findOne(new ObjectId(userId)).as(User.class);
        RequestResult<User> requestResult = new RequestResult<User>(user);
        return Response.ok(requestResult).build();
    }

    @GET
    @Path("/user/all")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("User:Read")
    public Response getAllUsers() throws Exception {
        MongoCursor<User> mongoCursor = userDb.find().projection("{password : 0}").as(User.class);
        ArrayList<User> users = Lists.newArrayList(mongoCursor.iterator());
        RequestResult<List<User>> requestResult = new RequestResult<List<User>>(users);
        return Response.ok(requestResult).build();
    }

    @POST
    @Path("/user")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("User:Edit")
    public Response addUser(User user) throws Exception {
        RequestResult<User> requestResult;
        if (user == null) {
            requestResult = new RequestResult<User>(AdminSpError.EMPTY_SUBMITTED_USER);
        } else  if (Strings.isNullOrEmpty(user.getEmailAddress())
            || Strings.isNullOrEmpty(user.getFirstName())
            || Strings.isNullOrEmpty(user.getLastName())
            || Strings.isNullOrEmpty(user.getPassword())
        ){
            requestResult = new RequestResult<User>(AdminSpError.USER_DATA_MISSING);
        } else {
            if(user.getPassword().length() < 8)
            {
                requestResult = new RequestResult<User>(AdminSpError.USER_PASSWORD_TOO_SHORT);
            } else {
                boolean isExist = userDb.findOne("{emailAddress : #}", user.getEmailAddress()).as(User.class) != null;
                if (isExist){
                    requestResult = new RequestResult<User>(AdminSpError.USER_EMAIL_WAS_USED);
                } else {
                    // set id to null
                    user.setId(null);
                    // hash password
                    String hashedPassword = Hashing.md5().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();
                    user.setPassword(hashedPassword);
                    // set create date
                    user.setCreateDate(new Date());

                    // save to db
                    userDb.insert(user);

                    user.setPassword(null);
                    requestResult = new RequestResult<User>(user);
                }
            }
        }
        return Response.ok(requestResult).build();
    }

    @PUT
    @Path("/user/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("User:Edit")
    public Response updateUser(@PathParam("userId") String userId, User updateUser) throws Exception {
        RequestResult<Void> requestResult;

        User user = userDb.findOne(new ObjectId(userId)).as(User.class);
        if (user == null){
            requestResult = new RequestResult<Void>(AdminSpError.USER_NOT_EXIST);
        } else {
            user.setEmailAddress(updateUser.getEmailAddress());
            user.setFirstName(updateUser.getFirstName());
            user.setLastName(updateUser.getLastName());
            user.setRoleIds(updateUser.getRoleIds());
            user.setModifiedDate(new Date());

            userDb.save(user);

            requestResult = new RequestResult<Void>(true);
        }
        return Response.ok(requestResult).build();
    }

    @DELETE
    @Path("/user/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("User:Edit")
    public Response deleteUser(@PathParam("userId") String userId) throws Exception {
        RequestResult<Void> requestResult;

        User user = userDb.findOne(new ObjectId(userId)).as(User.class);
        if (user == null){
            requestResult = new RequestResult<Void>(AdminSpError.USER_NOT_EXIST);
        } else {
            userDb.remove(new ObjectId(userId));
            requestResult = new RequestResult<Void>(true);
        }
        return Response.ok(requestResult).build();
    }

    @PUT
    @Path("/user/{userId}/roles")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("User:Edit")
    public Response setUserRoles(@PathParam("userId") String userId, List<String> roleIds) throws Exception {
        RequestResult<Void> requestResult;

        User user = userDb.findOne(new ObjectId(userId)).as(User.class);
        if (user == null){
            requestResult = new RequestResult<Void>(AdminSpError.USER_NOT_EXIST);
        } else {
            Iterable<ObjectId> objectIds = Iterables.transform(roleIds, new Function<String, ObjectId>() {
                public ObjectId apply(String roleId) {
                    return new ObjectId(roleId);
                }
            });
            ArrayList<ObjectId> roleObjectIds = Lists.newArrayList(objectIds);
            MongoCursor<Role> roles = roleDb.find("{_id : {$in : #}}", roleObjectIds).as(Role.class);
            if (roles.count() != roleIds.size()){
                requestResult = new RequestResult<Void>(AdminSpError.SOME_OF_ROLES_NOT_EXIST);
            } else {
                user.setRoleIds(roleObjectIds);
                userDb.save(user);

                requestResult = new RequestResult<Void>(true);
            }
        }

        return Response.ok(requestResult).build();
    }
}
