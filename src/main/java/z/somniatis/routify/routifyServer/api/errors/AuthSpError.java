package z.somniatis.routify.routifyServer.api.errors;

/**
 * User: Zun
 * Date: 8/4/2014
 * Time: 10:15 PM
 */
public class AuthSpError {
    public final static ErrorCode PASSWORD_N_EMAIL_REQUIRED = new ErrorCode(500, "Password and email are required.");
    public final static ErrorCode PASSWORD_N_EMAIL_INCORRECT = new ErrorCode(501, "Password or email is incorrect.");
    public final static ErrorCode REQUEST_WITH_INVALID_TOKEN = new ErrorCode(502, "Request has invalid security token.");
    public final static ErrorCode TOKEN_WAS_EXPIRED = new ErrorCode(503, "Token is expired. Login is required.");

    public final static ErrorCode PERMISSION_IS_REQUIRED = new ErrorCode(504, "Permission is required for using this function.");
    public final static ErrorCode ROLE_IS_REQUIRED = new ErrorCode(505, "Role is required for using this function.");
    public final static ErrorCode AUTHENTICATION_IS_REQUIRED = new ErrorCode(506, "Authentication is required for using this function.");
    public final static ErrorCode UNKNOWN_SERVER_ERROR_WHEN_AUTHORIZING = new ErrorCode(507, "Unknown error occurred when authorizing the usage of this function.");
}
