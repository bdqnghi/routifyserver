//package z.somniatis.routify.routifyServer.api;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.inject.Inject;
//import com.google.inject.servlet.RequestScoped;
//import org.mongodb.morphia.Datastore;
//import z.routify.routifymodel.shared.models.Hotel;
//import z.routify.routifymodel.shared.models.Location;
//import z.routify.routifymodel.shared.models.Poi;
//import z.routify.routifymodel.shared.models.PoiType;
//import z.routify.routifymodel.shared.models.agoda.AgodaHotel;
//import z.routify.routifymodel.shared.models.agoda.AgodaPoi;
//import z.somniatis.routify.routifyServer.api.config.BaseSP;
//import z.somniatis.routify.routifyServer.context.HttpContext;
//
//import javax.inject.Named;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * User: Zun
// * Date: 6/8/14
// * Time: 3:58 PM
// */
//@RequestScoped
//@Path("/AgodaGeo")
//public class AgodaGeoDataSP extends BaseSP {
//    @Inject
//    @Named("RoutifyDb")
//    Datastore routifyDb;
//    @Inject
//    private HttpContext context;
//
//    @POST
//    @Path("hotel")
//    @Produces(MediaType.APPLICATION_JSON)
//    public void addAgodaHotels(String hotelListJson) throws Exception {
//        try {
//            if (hotelListJson == null || hotelListJson.isEmpty()) {
//                System.out.println("addAgodaHotels : input is not valid");
//                return;
//            }
//
//            ObjectMapper objectMapper = new ObjectMapper();
//            Map<String, AgodaHotel> hotelMap = objectMapper.readValue(hotelListJson, new TypeReference<Map<String, AgodaHotel>>() {
//            });
//            for (AgodaHotel agodaHotel : hotelMap.values()) {
//                long originalId = Long.parseLong(agodaHotel.getHotelId());
//                String name = agodaHotel.getName();
//                String image = agodaHotel.getImage();
//                double rating = Double.parseDouble(agodaHotel.getStar().replaceAll("_", "."));
//                String description = agodaHotel.getDescription();
//                int commentCount = Integer.parseInt(agodaHotel.getComments());
//                Location location = new Location(Double.parseDouble(agodaHotel.getLongitude()), Double.parseDouble(agodaHotel.getLatitude()));
//                String detailLink = agodaHotel.getDetailLink();
//                String cityName = agodaHotel.getCityName();
//                String provinceName = agodaHotel.getProviceName();
//
//                Hotel hotel = new Hotel(
//                        originalId,
//                        name,
//                        image,
//                        rating,
//                        description,
//                        commentCount,
//                        location,
//                        detailLink,
//                        cityName,
//                        provinceName
//                );
//
//                routifyDb.save(hotel);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @POST
//    @Path("poi")
//    @Produces(MediaType.APPLICATION_JSON)
//    public void addPois(String poiListJson) throws Exception {
//        try {
//            if (poiListJson == null || poiListJson.isEmpty()) {
//                System.out.println("addPois : input is not valid");
//                return;
//            }
//
//            ObjectMapper objectMapper = new ObjectMapper();
//            List<AgodaPoi> agodaPois = objectMapper.readValue(poiListJson, new TypeReference<List<AgodaPoi>>() {});
//            List<PoiType> poiTypes = routifyDb.createQuery(PoiType.class).asList();
//            Map<Integer, PoiType> poiTypeMap = new HashMap<Integer, PoiType>();
//            for(PoiType poiType : poiTypes){
//                poiTypeMap.put(poiType.getPoiType(), poiType);
//            }
//
//            for (AgodaPoi agodaPoi : agodaPois) {
//                Poi poi =new Poi();
//                poi.setName(agodaPoi.getPoiName());
//                poi.setType(poiTypeMap.get(agodaPoi.getPoiType()));
//                poi.setLocation(new Location(Double.parseDouble(agodaPoi.getLongitude()), Double.parseDouble(agodaPoi.getLatitude())));
//                poi.setCity(agodaPoi.getCityName());
//                poi.setProvice(agodaPoi.getProviceName());
//                routifyDb.save(poi);
//            }
//
//            System.out.println("addPoiTypes succeeded.");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @POST
//    @Path("poitype")
//    @Produces(MediaType.APPLICATION_JSON)
//    public void addPoiTypes(String poitypeListJson) throws Exception {
//        try {
//            if (poitypeListJson == null || poitypeListJson.isEmpty()) {
//                System.out.println("addPoiTypes : input is not valid");
//                return;
//            }
//
//            ObjectMapper objectMapper = new ObjectMapper();
//            Map<String, PoiType> poiTypes = objectMapper.readValue(poitypeListJson, new TypeReference<Map<String, PoiType>>() {});
//
//            for (PoiType poiType : poiTypes.values()) {
//                routifyDb.save(poiType);
//            }
//
//            System.out.println("addPoiTypes succeeded.");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//}