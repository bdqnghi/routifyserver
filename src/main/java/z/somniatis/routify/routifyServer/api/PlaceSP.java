package z.somniatis.routify.routifyServer.api;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.mongodb.morphia.Datastore;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.routify.routifymodel.shared.models.PlaceEditHistory;
import z.routify.routifymodel.shared.models.PlaceEditHistoryItem;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.api.config.BaseSP;
import z.somniatis.routify.routifyServer.api.context.RequestContext;
import z.somniatis.routify.routifyServer.api.errors.ErrorCode;
import z.somniatis.routify.routifyServer.api.errors.PlaceSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.api.utils.DistrictGeoJsonUtil;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.regex.Pattern;

/**
 * User: Zun
 * Date: 6/8/14
 * Time: 3:58 PM
 */
@RequestScoped
@Path("place")
public class PlaceSP extends BaseSP {
    @Inject @Named("RoutifyDb") Datastore routifyDb;
    @Inject private DistrictGeoJsonUtil districtGeoJsonUtil;
    @Inject private Jongo jongo;
    @Inject private RequestContext requestContext;

    private ObjectMapper jsonMapper = new ObjectMapper();
    private MongoCollection placeDb;
    private MongoCollection placeHistoryDb;

    public PlaceSP() {
        placeDb = jongo.getCollection("placedetail");
        placeHistoryDb = jongo.getCollection("PlaceEditHistory");
    }

    public Jongo getJongo() {
        return jongo;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlace(@PathParam("id") String placeId){
        PlaceDetail placeDetail = placeDb.findOne(new ObjectId(placeId)).as(PlaceDetail.class);

        RequestResult<PlaceDetail> requestResult = new RequestResult<PlaceDetail>();
        requestResult.setResult(true);
        requestResult.setData(placeDetail);
        return Response.ok(requestResult).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Place:Edit")
    public Response updatePlace(@PathParam("id") String placeId, PlaceDetail updatedPlace){
        ObjectId placeObjectId = new ObjectId(placeId);
        PlaceDetail placeDetail = placeDb.findOne(placeObjectId).as(PlaceDetail.class);
        RequestResult<Void> requestResult = new RequestResult<Void>();
        try {
            if (placeDetail != null){
                // save place
                placeDb.update(placeDetail.getId()).with(updatedPlace);
                requestResult.setResult(true);

                // add place history
                String emailAddress = requestContext.getUser().getEmailAddress();
                saveEditHistory(placeObjectId, emailAddress, false);
            } else {
                requestResult.setResult(false);
                requestResult.setErrorCode(PlaceSpError.PLACE_ID_DOES_NOT_EXIST);
            }
        } catch (Exception e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            String message = "";
            for(StackTraceElement s : stackTrace){
                message += s.getClassName() + ":" + s.getMethodName() + ":" + s.getLineNumber() + "\n";
            }
            ErrorCode placeIdDoesNotExist = new ErrorCode(-1, message);
            requestResult.setResult(false);
            requestResult.setErrorCode(placeIdDoesNotExist);
        }

        return Response.ok(requestResult).build();
    }

    private void saveEditHistory(ObjectId placeObjectId, String emailAddress, boolean isNew) {
        PlaceEditHistory editHistory = placeHistoryDb.findOne("{placeId: #}", placeObjectId).as(PlaceEditHistory.class);
        if (editHistory == null){
            editHistory = new PlaceEditHistory();
            editHistory.setPlaceId(placeObjectId);
        }

        Date currentTime = new Date();
        List<PlaceEditHistoryItem> historyItems = editHistory.getHistoryItems();
        if (isNew){
            editHistory.setCreateDate(currentTime);
            editHistory.setCreatedUserEmail(emailAddress);
            historyItems.add(new PlaceEditHistoryItem(emailAddress, currentTime));
        } else {
            if (!historyItems.isEmpty()){
                PlaceEditHistoryItem lastItem = historyItems.get(historyItems.size() - 1);
                if (emailAddress.equals(lastItem.getEmail())){
                    if (currentTime.getTime() - lastItem.getEditTime().getTime() > 10 * 60 * 1000){
                        lastItem.setEditTime(currentTime);
                    } else historyItems.add(new PlaceEditHistoryItem(emailAddress, currentTime));
                } else historyItems.add(new PlaceEditHistoryItem(emailAddress, currentTime));
            } else historyItems.add(new PlaceEditHistoryItem(emailAddress, currentTime));
        }

        placeHistoryDb.save(editHistory);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Place:Edit")
    public Response addPlace(PlaceDetail addedPlace){
        ObjectId placeObjectId = new ObjectId();
        addedPlace.setId(placeObjectId);

        // save
        placeDb.save(addedPlace);

        // add place history
        String emailAddress = requestContext.getUser().getEmailAddress();
        saveEditHistory(placeObjectId, emailAddress, true);

        // return
        RequestResult<String> requestResult = new RequestResult<String>();
        requestResult.setResult(true);
        requestResult.setData(addedPlace.getId().toString());
        return Response.ok(requestResult).build();
    }

    /**
     * Get places by querying with params
     * @param lat (required):
     * @param lng (required):
     * @param categoriesParam (required):
     * @param sortedByParam (optional)
     *                 accept values:
     *                      'rating'(default): highest rating
     *                      'commented': most commented
     *                      'low_price' : lowest price
     *                      'high_price' : highest price
     * @param rangeParam (optional):
     *              default range is 10km
     * @param keywordParam (optional):
     *                if keyword is provided then searching for places which have name contains the keyword.
     * @return list of places which fulfill all the queried params
     */
    @GET
    @Path("location/query")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlacesByQueries(
            @QueryParam("lat") double lat,
            @QueryParam("lng") double lng,
            @DefaultValue("") @QueryParam("categories") String categoriesParam,
            @DefaultValue("-rating") @QueryParam("sortedBy") String sortedByParam, //rating, commented, price
            @DefaultValue("10.0") @QueryParam("range") Double rangeParam,
            @DefaultValue("") @QueryParam("keyword") String keywordParam
    ) throws Exception
    {

        List<PlaceDetail> placeDetails;

        if(sortedByParam.toLowerCase().equals("commented"))
            sortedByParam = "-review.length";
        else if(sortedByParam.toLowerCase().equals("rating"))
            sortedByParam = "-rating";
        else if(sortedByParam.toLowerCase().equals("low_price"))
            sortedByParam = "priceLevel";
        else if(sortedByParam.toLowerCase().equals("high_price"))
            sortedByParam = "-priceLevel";

        if(categoriesParam.toLowerCase().equals("all"))
        {
            placeDetails = routifyDb.createQuery(PlaceDetail.class).
                    disableValidation().
                    field("location").
                    near(lng, lat, rangeParam / 111.12).
                    field("name").
                    contains(keywordParam).
                    order(sortedByParam).
                    limit(30).
                    asList();
        }
        else
        {
            List<String> categories = Arrays.asList(categoriesParam.split(","));
            placeDetails = routifyDb.createQuery(PlaceDetail.class).
                                    disableValidation().
                                    field("types").
                                    hasAnyOf(categories).
                                    field("location").
                                    near(lng, lat, rangeParam / 111.12).
                                    field("name").
                                    contains(keywordParam).
                                    order(sortedByParam).
                                    limit(30).
                                    asList();
        }

        return Response.ok(placeDetails, MediaType.APPLICATION_JSON).build();
    }

    /**
     * Get places in district area
     * @param district (required)
     *      an_giang bac_giang bac_kan bac_lieu bac_ninh ben_tre binh_dinh binh_duong binh_phuoc
            binh_thuan can_tho cao_bang ca_mau dak_lak dak_nong da_nang dien_bien dong_nai dong_thap
            gia_lai hai_duong hai_phong HaNoi hau_giang ha_giang ha_nam ha_noi ha_tinh hoa_binh ho_chi_minh hung_yen
            khanh_hoa kien_giang kon_tum lai_chau lam_dong lang_son lao_cai long_an nam_dinh nghe_an ninh_binh ninh_thuan
            phu_tho phu_yen quang_binh quang_nam quang_ngai quang_ninh quang_tri soc_trang son_la tay_ninh thai_binh
            thai_nguyen thanh_hoa thua_thien_hue tien_giang tra_vinh tuyen_quang vinh_long vinh_phuc vung_tau yen_bai
     * @param categoriesParam (optional)
     * @param sortedByParam (optional)
     * @param keywordParam (optional)
     * @return list of places which fulfill all the queried params
     * @throws Exception
     */
    @GET
    @Path("district/query")
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Place:Read")
    public Response getPlacesInDistrictByQueries(
            @QueryParam("district") String district,
            @DefaultValue("") @QueryParam("categories") String categoriesParam,
            @DefaultValue("rating") @QueryParam("sortedBy") String sortedByParam, //rating, commented, price
            @DefaultValue("") @QueryParam("keyword") String keywordParam,
            @DefaultValue("") @QueryParam("searchArea") String searchAreaPolygonGeoJson
    ) throws Exception
    {
        // sort
        String sortToken = null;
        if(sortedByParam.toLowerCase().equals("commented"))
            sortToken = "{review.length : -1}";
        else if(sortedByParam.toLowerCase().equals("rating"))
            sortToken = "{rating : -1}";
        else if(sortedByParam.toLowerCase().equals("low_price"))
            sortToken = "{priceLevel : 1}";
        else if(sortedByParam.toLowerCase().equals("high_price"))
            sortToken = "{priceLevel: -1}";
        else sortToken = "{rating : -1}"; // default

        // within district area
        String withinToken;
        if (district.trim().isEmpty() || !StringUtils.isNullOrEmpty(searchAreaPolygonGeoJson)) {
            if (StringUtils.isNullOrEmpty(searchAreaPolygonGeoJson)){
                withinToken =  "";
            } else {
                withinToken = "location : { $geoWithin : { $geometry : " + searchAreaPolygonGeoJson + "}}";
            }
        }
        else {
            String geoJson = districtGeoJsonUtil.getDictrictShape(district);
            withinToken = "location : { $geoWithin : { $geometry : " + geoJson + "}}";
        }

        // categories
        List<String> categories = categoriesParam.trim().isEmpty() ? new ArrayList<String>() :  Arrays.asList(categoriesParam.split(","));
        String categoryToken = !categories.isEmpty()
                ? "types : { $in: " + jsonMapper.writeValueAsString(categories) + "}, "
                : "";

        // search keyword
        String keywordToken = !keywordParam.isEmpty()
                ? "name : {$regex: '"+ Pattern.compile(keywordParam)+"', $options: 'i'}, "
                : "";

        String queryContent = categoryToken + keywordToken + withinToken;
        if (queryContent.trim().endsWith(",")){
            queryContent = queryContent.substring(0, queryContent.length() - 1);
        }

        String query = "{" + queryContent + "}";

        MongoCursor<PlaceDetail> resultCollection = placeDb
                .find(query)
                .sort(sortToken)
                .projection("{name: 1, address:1, location:1, disable:1, phoneNumber:1, rating:1, priceLevel:1, types: 1}")
                .as(PlaceDetail.class);
        if (resultCollection.count() < 3000){
            List<PlaceDetail> placeDetails = Lists.newArrayList(resultCollection.iterator());
            RequestResult<List<PlaceDetail>> requestResult = new RequestResult<List<PlaceDetail>>();
            requestResult.setResult(true);
            requestResult.setData(placeDetails);
            requestResult.setErrorCode(null);
            return Response.ok(requestResult, MediaType.APPLICATION_JSON).build();
        } else {
            RequestResult<List<PlaceDetail>> requestResult = new RequestResult<List<PlaceDetail>>();
            requestResult.setResult(false);
            requestResult.setData(new ArrayList<PlaceDetail>());
            requestResult.setErrorCode(PlaceSpError.TOO_MANY_PLACES);

            return Response.ok(requestResult, MediaType.APPLICATION_JSON).build();
        }
    }
}