package z.somniatis.routify.routifyServer.api.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.jaxrs.json.JsonMapperConfigurator;
import com.fasterxml.jackson.jaxrs.json.MapperConfigExtention;
import com.google.inject.servlet.RequestScoped;
import org.bson.types.ObjectId;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;
import z.somniatis.routify.routifyServer.api.config.filters.CharsetResponseFilter;
import z.somniatis.routify.routifyServer.api.config.filters.ContextBindingFilter;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;

import javax.inject.Inject;

public class Jersey2Application extends ResourceConfig {

    @Inject
    public Jersey2Application(ServiceLocator serviceLocator) {
        // Set package to look for resources in
        packages("z.somniatis.routify.routifyServer.api");
        register(ContextBindingFilter.class);
        register(CharsetResponseFilter.class);
        JsonMapperConfigurator.setMapperConfigExtension(new MapperConfigExtention() {
            @Override
            public void extendConfiguration(ObjectMapper objectMapper) {
                SimpleModule simpleModule = new SimpleModule("RoutifyServerModule", new Version(1, 0, 0, null));
                simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
                simpleModule.addDeserializer(ObjectId.class, new ObjectIdDeserializer());
                objectMapper.registerModule(simpleModule);
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            }
        });

        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(GuiceServletConfig.INJECTOR);
    }
}