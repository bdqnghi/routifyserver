package z.somniatis.routify.routifyServer.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.mongodb.morphia.Datastore;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.routify.routifymodel.shared.models.PlaceEditHistory;
import z.routify.routifymodel.shared.models.User;
import z.routify.routifymodel.shared.models.user.Role;
import z.somniatis.routify.routifyServer.annotations.RequiresAuthentication;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.api.config.BaseSP;
import z.somniatis.routify.routifyServer.api.context.RequestContext;
import z.somniatis.routify.routifyServer.api.errors.ErrorCode;
import z.somniatis.routify.routifyServer.api.errors.PlaceSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.api.errors.UserSpError;

import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// http://127.0.0.1:8888/api/User/
@RequestScoped
@Path("user")
public class UserSP extends BaseSP {
    @Inject private Jongo jongo;
    @Inject private RequestContext requestContext;

    private final MongoCollection permissionDb;
    private final MongoCollection roleDb;
    private final MongoCollection userDb;
    private final MongoCollection placeDb;
    private final MongoCollection placeHistoryDb;

    public UserSP() {
        permissionDb = jongo.getCollection("permission");
        roleDb = jongo.getCollection("role");
        userDb = jongo.getCollection("user");
        placeDb = jongo.getCollection("placedetail");
        placeHistoryDb = jongo.getCollection("PlaceEditHistory");
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    public Response getCurrentUser() throws Exception {
        User user = requestContext.getUser();
        List<ObjectId> roleIds = user.getRoleIds();
        Iterator<Role> iterator = roleDb.find("{_id: {$in: #}}", roleIds).as(Role.class).iterator();
        ArrayList<Role> roles = Lists.newArrayList(iterator);

        List<ObjectId> permissionIds = new ArrayList<ObjectId>();
        List<String> roleNames = new ArrayList<String>();
        for(Role role : roles){
            roleNames.add(role.getName());
            permissionIds.addAll(role.getPermissionIds());
        }

        List<String> permissionNames = permissionDb.distinct("name").query("{_id: {$in: #}}", permissionIds).as(String.class);

        ArrayList result = new ArrayList();
        result.add(user);
        result.add(roleNames);
        result.add(permissionNames);
        RequestResult<ArrayList> requestResult = new RequestResult<ArrayList>(result);
        return Response.ok(requestResult).build();
    }


    @GET
    @Path("/editedPlaces")
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresPermissions("Place:Edit")
    public Response getUserEditedPlaces(){
        User user = requestContext.getUser();
        String emailAddress = user.getEmailAddress();

        Iterator<PlaceEditHistory> historyIterator = placeHistoryDb.find("{$or: [{createdUserEmail: #},{'historyItems.email' : #}]}", emailAddress, emailAddress).as(PlaceEditHistory.class).iterator();
        ArrayList<PlaceEditHistory> placeEditHistories = Lists.newArrayList(historyIterator);

        List<ObjectId> placeIds = Lists.transform(placeEditHistories, new Function<PlaceEditHistory, ObjectId>() {
            @Override
            public ObjectId apply(PlaceEditHistory placeEditHistory) {
                return placeEditHistory.getPlaceId();
            }
        });

        Iterator<PlaceDetail> iterator = placeDb.find("{_id: {$in: #}}", placeIds).as(PlaceDetail.class).iterator();

        ArrayList<PlaceDetail> placeDetails = Lists.newArrayList(iterator);
        RequestResult<List<PlaceDetail>> requestResult = new RequestResult<List<PlaceDetail>>(placeDetails);
        return Response.ok(requestResult).build();
    }

}