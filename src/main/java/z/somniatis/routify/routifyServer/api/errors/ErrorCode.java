package z.somniatis.routify.routifyServer.api.errors;

/**
 * User: Zun
 * Date: 8/4/2014
 * Time: 9:42 PM
 */
public class ErrorCode {
    private int errorCode;
    private String errorMessage;

    public ErrorCode(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}