package z.somniatis.routify.routifyServer.api.errors;

/**
 * User: Zun
 * Date: 8/4/2014
 * Time: 9:40 PM
 */
public class RequestResult<T> {
    private boolean result;
    private ErrorCode errorCode;
    private T data;

    public RequestResult() {
    }

    public RequestResult(boolean result) {
        this.result = result;
    }

    public RequestResult(ErrorCode errorCode) {
        this.result = false;
        this.errorCode = errorCode;
    }

    public RequestResult(T data) {
        this.result = true;
        this.data = data;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}