package z.somniatis.routify.routifyServer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Injector;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.junit.Assert;
import org.junit.Test;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.logging.LogRecord;

import static org.hamcrest.core.Is.is;

public class PlaceApiTest extends JerseyTest {
    static final Injector INJECTOR = new GuiceServletConfig().getInjector();
    private RoutifyTestApplication routifyTestApplication;
    private ObjectMapper jsonMapper = new ObjectMapper();
    private String currentToken;

    public PlaceApiTest() throws TestContainerException {
        ApplicationHandler applicationHandler = new ApplicationHandler(routifyTestApplication);
        ServiceLocator serviceLocator = applicationHandler.getServiceLocator();
        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(INJECTOR);
    }

    @Override
    protected Application configure() {
        routifyTestApplication = new RoutifyTestApplication();
        return routifyTestApplication;
    }

    @Test
    public void login() throws IOException {
       Response response = target("auth/login").path("")
               .queryParam("email", "nghia910@gmail.com")
               .queryParam("password", "qwerasdf")
               .queryParam("remember", false)
               .request(MediaType.APPLICATION_JSON_TYPE).get();
        String json = response.readEntity(String.class);
        RequestResult<String> requestResult = jsonMapper.readValue(json, new TypeReference<RequestResult<String>>() {});

        currentToken = requestResult.getData();
        System.out.println(currentToken);
    }

    @Test
    public void testUpdatePlace_EditHistoryIsAdded() throws IOException {
        login();
        Response response = target("place/53f65ef2f149489a76cbaac5")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String json = response.readEntity(String.class);
        RequestResult<PlaceDetail> requestResult = jsonMapper.readValue(json, new TypeReference<RequestResult<PlaceDetail>>() {});
        PlaceDetail placeDetail = requestResult.getData();
        System.out.println(placeDetail.getName());

        response = target("place/53f65ef2f149489a76cbaac5")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "Bearer " + currentToken)
                .put(Entity.json(placeDetail), Response.class);
        json = response.readEntity(String.class);
        System.out.println(json);
        requestResult = jsonMapper.readValue(json, RequestResult.class);
        Assert.assertThat(requestResult.getResult(), is(true));
    }

    @Test
    public void testAddPlace_EditHistoryIsAdded() throws IOException {
        login();
        Response response = target("place/53f65ef2f149489a76cbaac5")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        String json = response.readEntity(String.class);
        RequestResult<PlaceDetail> requestResult = jsonMapper.readValue(json, new TypeReference<RequestResult<PlaceDetail>>() {});
        PlaceDetail placeDetail = requestResult.getData();
        System.out.println(placeDetail.getName());
        placeDetail.setId(null);
        response = target("place")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "Bearer " + currentToken)
                .post(Entity.json(placeDetail), Response.class);
        json = response.readEntity(String.class);
        System.out.println(json);
        requestResult = jsonMapper.readValue(json, RequestResult.class);
        Assert.assertThat(requestResult.getResult(), is(true));
    }

    @Test
    public void testRefresh() {
       Response response = target("auth/refresh")
               .request(MediaType.APPLICATION_JSON_TYPE)
               .header("Authorization", "Bearer " + currentToken)
               .get();
        String requestResult = response.readEntity(String.class);
        System.out.println(requestResult);
    }

    @Test
    public void testGetUsers() {
       Response response = target("admin/user/all")
               .request(MediaType.APPLICATION_JSON_TYPE)
               .header("Authorization", "Bearer " + currentToken)
               .get();
        String requestResult = response.readEntity(String.class);
        System.out.println(requestResult);
    }

}