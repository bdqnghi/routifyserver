import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.Test;

import java.security.SecureRandom;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: Zun
 * Date: 9/12/2014
 * Time: 7:16 PM
 */
public class JWT {
    @Test
    public void test() throws JOSEException, ParseException {
        // Generate random 32-bit shared secret
        SecureRandom random = new SecureRandom();
        byte[] sharedSecret = new byte[32];
        random.nextBytes(sharedSecret);

        // Create HMAC signer
        JWSSigner signer = new MACSigner(sharedSecret);

        // Prepare JWT with claims set
        JWTClaimsSet claimsSet = new JWTClaimsSet();
        claimsSet.setSubject("alice");
        claimsSet.setIssuer("https://c2id.com");

        List<String> aud = new ArrayList<String>();
        aud.add("https://app-one.com");
        aud.add("https://app-two.com");
        claimsSet.setAudience(aud);
        // Set expiration in 10 minutes
        claimsSet.setExpirationTime(new Date(new Date().getTime() + 1000*60*10));
        claimsSet.setNotBeforeTime(new Date());
        claimsSet.setIssueTime(new Date());
        claimsSet.setJWTID(UUID.randomUUID().toString());

        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

        // Apply the HMAC
        signedJWT.sign(signer);

        // To serialize to compact form, produces something like
        // eyJhbGciOiJIUzI1NiJ9.SGVsbG8sIHdvcmxkIQ.onO9Ihudz3WkiauDO2Uhyuz0Y18UASXlSc1eS0NkWyA
        String s = signedJWT.serialize();

        // To parse the JWS and verify it, e.g. on client-side
        signedJWT = SignedJWT.parse(s);

        JWSVerifier verifier = new MACVerifier(sharedSecret);

        assertTrue(signedJWT.verify(verifier));

        // Retrieve the JWT claims
        assertEquals("alice", signedJWT.getJWTClaimsSet().getSubject());
    }
}
