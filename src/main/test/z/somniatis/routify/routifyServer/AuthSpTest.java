package z.somniatis.routify.routifyServer;

import com.google.inject.Injector;
import org.bson.types.ObjectId;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.internal.MapPropertiesDelegate;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.hamcrest.core.Is;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.junit.Assert;
import org.junit.Test;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;
import z.routify.routifymodel.shared.models.user.Role;
import z.somniatis.routify.routifyServer.annotations.Logical;
import z.somniatis.routify.routifyServer.annotations.RequiresPermissions;
import z.somniatis.routify.routifyServer.annotations.processors.RequiresPermissionsProcessor;
import z.somniatis.routify.routifyServer.api.AuthorizationSP;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.*;

public class AuthSpTest extends JerseyTest {
    static final Injector INJECTOR = new GuiceServletConfig().getInjector();
    private RoutifyTestApplication routifyTestApplication;
    private String currentToken;

    public AuthSpTest() throws TestContainerException {
        ApplicationHandler applicationHandler = new ApplicationHandler(routifyTestApplication);
        ServiceLocator serviceLocator = applicationHandler.getServiceLocator();
        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(INJECTOR);
    }

    @Override
    protected Application configure() {
        routifyTestApplication = new RoutifyTestApplication();
        return routifyTestApplication;
    }

    @Test
    public void testLogin() {
       Response response = target("auth/login").path("")
               .queryParam("emailAddress", "nghia910@gmail.com")
               .queryParam("password", "qwerasdf")
               .queryParam("remember", false)
               .request(MediaType.APPLICATION_JSON_TYPE).get();
        RequestResult<String> requestResult = response.readEntity(RequestResult.class);
        currentToken = requestResult.getData();
        System.out.println(currentToken);
    }

    @Test
    public void testRefresh() {
       Response response = target("auth/refresh")
               .request(MediaType.APPLICATION_JSON_TYPE)
               .header("Authorization", "Bearer " + currentToken)
               .get();
        String requestResult = response.readEntity(String.class);
        System.out.println(requestResult);
    }

    @Test
    public void testRevoke() {
       Response response = target("auth/revoke")
               .request(MediaType.APPLICATION_JSON_TYPE)
               .header("Authorization", "Bearer "+ currentToken)
               .get();
        String requestResult = response.readEntity(String.class);
        System.out.println(requestResult);
    }

    @Test
    public void testGetUsers() {
       Response response = target("admin/user/all")
               .request(MediaType.APPLICATION_JSON_TYPE)
               .header("Authorization", "Bearer " + currentToken)
               .get();
        String requestResult = response.readEntity(String.class);
        System.out.println(requestResult);
    }

    @Test
    public void testCase1() {
       testLogin();
       testRefresh();
       testRevoke();
    }

    @Test
    public void testCase2() {
       testLogin();
       testGetUsers();
    }

    @Test
    public void testRequiresPermissionsProcessor() {
       testLogin();
        RequiresPermissionsProcessor processor = new RequiresPermissionsProcessor();
        INJECTOR.injectMembers(processor);

        RequiresPermissions requiresPermissions = new RequiresPermissions(){
            @Override
            public String[] value() {
                return new String[]{"AllPermission"};
            }

            @Override
            public Logical logical() {
                return Logical.AND;
            }

            @Override
            public Class<? extends Annotation> annotationType() {
                return RequiresPermissions.class;
            }
        };
        ContainerRequest headers = new ContainerRequest(null, null, null, null , new MapPropertiesDelegate());
        headers.header("Authorization", AuthorizationSP.BEARER + currentToken);
        RequestResult process = processor.process(requiresPermissions, null, headers);
        Assert.assertThat(process.getResult(), is(true));
    }
}