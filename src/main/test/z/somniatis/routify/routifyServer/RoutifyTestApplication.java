package z.somniatis.routify.routifyServer;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.jaxrs.json.JsonMapperConfigurator;
import com.fasterxml.jackson.jaxrs.json.MapperConfigExtention;
import org.bson.types.ObjectId;
import org.glassfish.jersey.server.ResourceConfig;
import z.somniatis.routify.routifyServer.api.config.filters.CharsetResponseFilter;
import z.somniatis.routify.routifyServer.api.config.filters.ContextBindingFilter;
import z.somniatis.routify.routifyServer.api.config.ObjectIdDeserializer;
import z.somniatis.routify.routifyServer.api.config.ObjectIdSerializer;

/**
 * This class is specified in the <tt>web.xml</tt> file, and is used when Jersey is loaded to determine where resources
 * (API classes) are located, and for any other dependencies.
 * 
 * @author Angus Macdonald (amacdonald@aetherworks.com)
 */
public class RoutifyTestApplication extends ResourceConfig {

	/**
	 * This is the constructor called by the application server when loading the application (as specified by the
	 * web.xml).
	 */
	public RoutifyTestApplication() {

        // Set package to look for resources in
        packages("z.somniatis.routify.routifyServer.api");
        register(ContextBindingFilter.class);
        register(CharsetResponseFilter.class);
        JsonMapperConfigurator.setMapperConfigExtension(new MapperConfigExtention() {
            @Override
            public void extendConfiguration(ObjectMapper objectMapper) {
                SimpleModule simpleModule = new SimpleModule("RoutifyServerModule", new Version(1, 0, 0, null));
                simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
                simpleModule.addDeserializer(ObjectId.class, new ObjectIdDeserializer());
                objectMapper.registerModule(simpleModule);
            }
        });
	}
}