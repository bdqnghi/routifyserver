package z.somniatis.routify.routifyServer;

import com.google.inject.Module;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.junit.Test;
import z.routify.routifymodel.shared.models.User;
import z.routify.routifymodel.shared.models.user.Permission;
import z.routify.routifymodel.shared.models.user.Role;
import z.somniatis.routify.routifyServer.api.AdminSP;
import z.somniatis.routify.routifyServer.api.errors.AdminSpError;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.guiceModules.DbModule;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;
import z.somniatis.routify.routifyServer.guiceModules.InjectorProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsNull.*;

/**
 * User: Zun
 * Date: 7/9/2014
 * Time: 9:41 PM
 */
public class AdminApiTest {

    private final AdminSP adminSP;
    private final Jongo jongo;
    Random random = new Random();
    public AdminApiTest()
    {
        List<Module> guiceModules = new ArrayList<Module>();
        guiceModules.add(new DbModule());
        InjectorProvider.init(guiceModules);

        GuiceServletConfig.INJECTOR = InjectorProvider.getInjector();
        adminSP = new AdminSP();
        jongo = GuiceServletConfig.INJECTOR.getInstance(Jongo.class);
    }

    @Test
    public void testAddPermission_NewPermissionIsAddedToDb() throws Exception{
        Permission permission = new Permission();
        permission.setName("permission" + random.nextLong());
        RequestResult<Permission> result = (RequestResult<Permission>) adminSP.addPermission(permission).getEntity();

        assertThat(result.getResult(), is(true));
        assertThat(result.getData(), notNullValue());
        assertThat(result.getData().getId(), notNullValue());
    }

    @Test
    public void testUpdatePermission_NewPermissionNameIsSaved() throws Exception{
        //add
        Permission permission = new Permission();
        permission.setName("permission" + random.nextLong());
        RequestResult<Permission> result = (RequestResult<Permission>) adminSP.addPermission(permission).getEntity();

        // update
        ObjectId id = result.getData().getId();
        String newName = "permission" + random.nextLong();
        permission.setName(newName);
        result = (RequestResult<Permission>) adminSP.updatePermission(id.toString(), permission).getEntity();
        assertThat(result.getResult(), is(true));

        // check
        result = (RequestResult<Permission>) adminSP.getPermission(id.toString()).getEntity();
        assertThat(result.getResult(), is(true));
        assertThat(result.getData(), notNullValue());
        assertThat(result.getData().getName(), is(newName));

    }

    @Test
    public void testDeletePermission_PermissionIsRemoved() throws Exception{
        //add
        Permission permission = new Permission();
        permission.setName("permission" + random.nextLong());
        RequestResult<Permission> result = (RequestResult<Permission>) adminSP.addPermission(permission).getEntity();

        // remove
        ObjectId id = result.getData().getId();
        result = (RequestResult<Permission>) adminSP.deletePermission(id.toString()).getEntity();
        assertThat(result.getResult(), is(true));

        // check
        result = (RequestResult<Permission>) adminSP.getPermission(id.toString()).getEntity();
        assertThat(result.getResult(), is(true));
        assertThat(result.getData(), nullValue());
    }

    @Test
    public void testAddUser_UserWithMissingDataWouldReturnError() throws Exception{
        User user = new User();
        RequestResult<User> result = (RequestResult<User>) adminSP.addUser(user).getEntity();

        assertThat(result.getResult(), is(false));
        assertThat(result.getErrorCode(), is(AdminSpError.USER_DATA_MISSING));
   }

    @Test
    public void testAddUser_NullUserWouldReturnError() throws Exception{
        User user = null;
        RequestResult<User> result = (RequestResult<User>) adminSP.addUser(user).getEntity();

        assertThat(result.getResult(), is(false));
        assertThat(result.getErrorCode(), is(AdminSpError.EMPTY_SUBMITTED_USER));
   }

    @Test
    public void testAddUser_PasswordUserShorterThan8WouldReturnError() throws Exception{
        User user = new User();
        user.setFirstName("user" + random.nextLong());
        user.setLastName("user" + random.nextLong());
        user.setPassword("1234");
        user.setEmailAddress("nghia@mail.com");

        RequestResult<User> result = (RequestResult<User>) adminSP.addUser(user).getEntity();

        assertThat(result.getResult(), is(false));
        assertThat(result.getErrorCode(), is(AdminSpError.USER_PASSWORD_TOO_SHORT));
   }

    @Test
    public void testAddUser_NormalUserIsAddedToDb() throws Exception{
        User user = new User();
        user.setFirstName("user" + random.nextLong());
        user.setLastName("user" + random.nextLong());
        user.setPassword("12345678");
        user.setEmailAddress(random.nextLong() + "@mail.com");

        RequestResult<User> result = (RequestResult<User>) adminSP.addUser(user).getEntity();

        assertThat(result.getResult(), is(true));
        assertThat(result.getData().getId(), notNullValue());
   }

    @Test
    public void testSetUserRoles_NotExistUserWouldReturnError() throws Exception{
        String userId = new ObjectId().toHexString();
        RequestResult<Void> result = (RequestResult<Void>) adminSP.setUserRoles(userId, new ArrayList<String>()).getEntity();

        assertThat(result.getResult(), is(false));
        assertThat(result.getErrorCode(), is(AdminSpError.USER_NOT_EXIST));
   }

    @Test
    public void testSetUserRoles_NotExistRolesWouldReturnError() throws Exception{
        User user = new User();
        user.setFirstName("user" + random.nextLong());
        user.setLastName("user" + random.nextLong());
        user.setPassword("12345678");
        user.setEmailAddress(random.nextLong() + "@mail.com");

        RequestResult<User> userRequestResult = (RequestResult<User>) adminSP.addUser(user).getEntity();

        String userId = userRequestResult.getData().getId().toHexString();

        ArrayList<String> roleIds = new ArrayList<String>();
        roleIds.add(new ObjectId().toHexString());
        RequestResult<Void> roleRequestResult = (RequestResult<Void>) adminSP.setUserRoles(userId, roleIds).getEntity();

        assertThat(roleRequestResult.getResult(), is(false));
        assertThat(roleRequestResult.getErrorCode(), is(AdminSpError.SOME_OF_ROLES_NOT_EXIST));
    }

    @Test
    public void testSetUserRoles_CorrectRolesAndUserWouldUpdateDb() throws Exception{
        // add user
        User user = new User();
        user.setFirstName("user" + random.nextLong());
        user.setLastName("user" + random.nextLong());
        user.setPassword("12345678");
        user.setEmailAddress(random.nextLong() + "@mail.com");

        RequestResult<User> userRequestResult = (RequestResult<User>) adminSP.addUser(user).getEntity();
        String userId = userRequestResult.getData().getId().toHexString();

        // add role
        Role role = new Role();
        role.setName("role" + random.nextLong());
        RequestResult<Role> addRoleResult = (RequestResult<Role>) adminSP.addRole(role).getEntity();
        String roleId = addRoleResult.getData().getId().toString();

        // set roles for user
        ArrayList<String> roleIds = new ArrayList<String>();
        roleIds.add(roleId);
        RequestResult<Void> roleRequestResult = (RequestResult<Void>) adminSP.setUserRoles(userId, roleIds).getEntity();
        assertThat(roleRequestResult.getResult(), is(true));

        // check user
        userRequestResult = (RequestResult<User>) adminSP.getUser(userId).getEntity();
        user = userRequestResult.getData();
        assertThat(user, notNullValue());
        assertThat(user.getRoleIds().size(), is(roleIds.size()));
        assertThat(user.getRoleIds().get(0).toHexString(), is(roleId));
    }

    @Test
    public void generateUserData() throws Exception{
        // add permission
        Permission permission = new Permission();
        permission.setName("AllPermission");
        RequestResult<Permission> permissionRequestResult = (RequestResult<Permission>) adminSP.addPermission(permission).getEntity();
        ObjectId permissionId = permissionRequestResult.getData().getId();

        // add role
        Role role = new Role();
        role.setName("root");
        ArrayList<ObjectId> permissionIds = new ArrayList<ObjectId>();
        permissionIds.add(permissionId);
        role.setPermissionIds(permissionIds);

        RequestResult<Role> roleRequestResult = (RequestResult<Role>) adminSP.addRole(role).getEntity();
        ObjectId roleId = roleRequestResult.getData().getId();

        // add user
        User user = new User();
        user.setFirstName("Nghia");
        user.setLastName("Nguyen");
        user.setPassword("qwerasdf");
        user.setEmailAddress("nghia910@gmail.com");
        ArrayList<ObjectId> roleIds = new ArrayList<ObjectId>();
        roleIds.add(roleId);
        user.setRoleIds(roleIds);

        RequestResult<User> userRequestResult = (RequestResult<User>) adminSP.addUser(user).getEntity();
        String userId = userRequestResult.getData().getId().toString();

        System.out.println(userId);
        System.out.println(roleId.toHexString());
        System.out.println(permissionId.toHexString());
    }

}