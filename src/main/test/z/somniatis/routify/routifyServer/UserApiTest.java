package z.somniatis.routify.routifyServer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Injector;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.junit.Test;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.somniatis.routify.routifyServer.api.errors.RequestResult;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
/**
* User: Zun
* Date: 7/9/2014
* Time: 9:41 PM
*/
public class UserApiTest  extends JerseyTest {
    static final Injector INJECTOR = new GuiceServletConfig().getInjector();
    private RoutifyTestApplication routifyTestApplication;
    private ObjectMapper jsonMapper = new ObjectMapper();
    private String currentToken;

    public UserApiTest() throws TestContainerException {
        ApplicationHandler applicationHandler = new ApplicationHandler(routifyTestApplication);
        ServiceLocator serviceLocator = applicationHandler.getServiceLocator();
        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(INJECTOR);
    }

    @Override
    protected Application configure() {
        routifyTestApplication = new RoutifyTestApplication();
        return routifyTestApplication;
    }

    @Test
    public void login() throws IOException {
        Response response = target("auth/login").path("")
                .queryParam("email", "nghia910@gmail.com")
                .queryParam("password", "qwerasdf")
                .queryParam("remember", false)
                .request(MediaType.APPLICATION_JSON_TYPE).get();
        String json = response.readEntity(String.class);
        RequestResult<String> requestResult = jsonMapper.readValue(json, new TypeReference<RequestResult<String>>() {});

        currentToken = requestResult.getData();
        System.out.println(currentToken);
    }

    @Test
    public void testGetEditedPlaces() throws IOException {
        login();
        Response response = target("user/editedPlaces")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "Bearer " + currentToken)
                .get();
        String json = response.readEntity(String.class);

        System.out.println(json);
        RequestResult<List<PlaceDetail>> requestResult = jsonMapper.readValue(json, new TypeReference<RequestResult<List<PlaceDetail>>>() {});
        List<PlaceDetail> placeDetails = requestResult.getData();


        assertThat(placeDetails.size() > 0, is(true));
    }
}