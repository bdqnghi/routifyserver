package z.somniatis.routify.routifyServer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Module;
import org.bson.types.ObjectId;
import org.hamcrest.core.IsNot;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import z.routify.routifymodel.shared.models.PlaceDetail;
import z.somniatis.routify.routifyServer.api.PlaceSP;
import z.somniatis.routify.routifyServer.guiceModules.DbModule;
import z.somniatis.routify.routifyServer.guiceModules.GuiceServletConfig;
import z.somniatis.routify.routifyServer.guiceModules.InjectorProvider;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * User: Zun
 * Date: 7/9/2014
 * Time: 9:41 PM
 */
public class PlaceSpTest {
    private final PlaceSP placeSP;
    private final String defaultCategories = "";
    private final String defaultSortedBy = "-rating";
    private final double defaultRange = 10.0;
    private final String defaultKeyword = "";

    public PlaceSpTest() {
        List<Module> guiceModules = new ArrayList<Module>();
        guiceModules.add(new DbModule());
        InjectorProvider.init(guiceModules);

        GuiceServletConfig.INJECTOR = InjectorProvider.getInjector();
        placeSP = new PlaceSP();
    }


    @Test
    public void testGetPlacesByLocation() throws Exception {
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, "all", defaultSortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(30));
    }

    @Test
    public void testGetPlacesByLocationAnCategory_EmptyCategories() throws Exception {
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, "", defaultSortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(0));
    }

    @Test
    public void testGetPlacesByLocationAnCategory_WithOneCategory() throws Exception {
        String category = "gas_station";
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, category, defaultSortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(9));

        for (PlaceDetail place : places) {
            assertThat(place.getTypes().contains(category), is(true));
        }
    }

    @Test
    public void testGetPlacesByLocationAnCategory_WithManyCategory() throws Exception {
        String category = "gas_station,museum";
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, category, defaultSortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(11));

        for (PlaceDetail place : places) {
            assertThat((place.getTypes().contains("gas_station")
                    || place.getTypes().contains("museum")), is(true));
        }
    }

    @Test
    public void testGetPlacesByLocationAndCategoriesWithHighestRating_AllCategories() throws Exception {
        String category = "all";
        String sortedBy = "rating";
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, category, sortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(30));

        PlaceDetail previousPlace = places.get(0);
        for (int i = 1; i < places.size(); i++) {
            PlaceDetail currentPlace = places.get(i);
            assertThat(previousPlace.getRating() >= currentPlace.getRating(), is(true));
            previousPlace = currentPlace;
        }
    }

    @Test
    public void testGetPlacesByLocationAndCategoriesWithHighestRating_SomeCategories() throws Exception {
        String category = "gas_station,museum";
        String sortedBy = "rating";
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, category, sortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(11));

        PlaceDetail previousPlace = places.get(0);
        for (int i = 1; i < places.size(); i++) {
            PlaceDetail currentPlace = places.get(i);
            assertThat(previousPlace.getRating() >= currentPlace.getRating(), is(true));
            previousPlace = currentPlace;
        }
    }

    @Test
    public void testGetPlacesByLocationAndCategoriesWithHighestRating_NoCategories() throws Exception {
        String sortedBy = "rating";
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, defaultCategories, sortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(0));
    }

    @Test
    public void testGetPlacesByLocationAndCategoriesWithMostCommented_AllCategories() throws Exception {
        String category = "all";
        String sortedBy = "commented";
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, category, sortedBy, defaultRange, defaultKeyword);
        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(30));

        PlaceDetail previousPlace = places.get(0);

        for (int i = 1; i < places.size(); i++) {
            int previousPlaceCommentsNumber = 0;
            int currentPlaceCommentsNumber = 0;

            PlaceDetail currentPlace = places.get(i);
            if (currentPlace.getReviews() != null)
                currentPlaceCommentsNumber = currentPlace.getReviews().size();

            if (previousPlace.getReviews() != null)
                previousPlaceCommentsNumber = previousPlace.getReviews().size();

            assertThat(previousPlaceCommentsNumber >= currentPlaceCommentsNumber, is(false));
            previousPlace = currentPlace;
        }
    }

    @Test
    public void testGetPlacesWithinRange() throws Exception {
        String category = "all";
        double range = 5.0;
        Response placesByLocation = placeSP.getPlacesByQueries(11.945834, 108.434408, category, defaultSortedBy, range, defaultKeyword);

        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();
        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(30));

        for (int i = 0; i < places.size(); i++) {
            PlaceDetail currentPlace = places.get(i);
            double distance = HaversineInKM(11.945834, 108.434408,
                    currentPlace.getLocation().getLat(),
                    currentPlace.getLocation().getLng());
            assertThat(distance <= range, is(true));
        }
    }

    @Test
    public void testGetPlacesByKeywordAndCategoriesAndLatLng() throws Exception {

        String category = "all";
        String keyword = "Petrolimex";
        Response placesByLocation = placeSP.getPlacesByQueries(10.775290, 106.698819, category, defaultSortedBy, defaultRange, keyword);

        List<PlaceDetail> places = (List<PlaceDetail>) placesByLocation.getEntity();

        assertThat(placesByLocation.getStatus(), is(200));
        assertThat(places.size(), is(24));
        for (int i = 0; i < places.size(); i++) {
            PlaceDetail currentPlace = places.get(i);
            assertThat(currentPlace.getName().contains(keyword), is(true));
        }
    }

    @Test
    public void testGetPlacesByCategoriesWithinDistrictArea() throws Exception {
        String quang_ninh = "quang_ninh";

        Response placesInDistrictByQueries = placeSP.getPlacesInDistrictByQueries(quang_ninh, "gas_station,atm", "commented", "", "");
        List<PlaceDetail> places = (List<PlaceDetail>) placesInDistrictByQueries.getEntity();

        assertThat(placesInDistrictByQueries.getStatus(), is(200));
        System.out.println(places.size());
        assertEquals(places.size() > 0, true);

        for (PlaceDetail place : places) {
            assertThat(place.getTypes().contains("gas_station") || place.getTypes().contains("atm"), is(true));
            System.out.println(place.getAddress());
        }
    }

    @Test
    public void testGetPlacesByKeywordWithinDistrictArea() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        String quang_ninh = "quang_ninh";

        Response placesInDistrictByQueries = placeSP.getPlacesInDistrictByQueries(quang_ninh, "", "commented", "Chùa", "");
        List<PlaceDetail> places = (List<PlaceDetail>) placesInDistrictByQueries.getEntity();

        assertThat(placesInDistrictByQueries.getStatus(), is(200));
        System.out.println(places.size());
        assertEquals(places.size() > 0, true);

        for (PlaceDetail place : places) {
            assertThat(place.getName().toLowerCase().contains("chùa"), is(true));
        }
    }

    @Test
    public void testGetPlacesByKeywordAndCategoriesWithinDistrictArea() throws Exception {
        String quang_ninh = "quang_ninh";

        Response placesInDistrictByQueries = placeSP.getPlacesInDistrictByQueries(quang_ninh, "atm", "commented", "Bưu Điện Móng Cái", "");
        List<PlaceDetail> places = (List<PlaceDetail>) placesInDistrictByQueries.getEntity();

        assertThat(placesInDistrictByQueries.getStatus(), is(200));
        System.out.println(places.size());
        assertEquals(places.size() > 0, true);

        for (PlaceDetail place : places) {
            System.out.println(place.getName());
            assertThat(place.getName().toLowerCase().contains("ngân hàng"), is(true));
            assertThat(place.getTypes().contains("atm"), is(true));
        }
    }

    @Test
    public void getPlaceById_WrongIdReturnNull(){
        Response place = placeSP.getPlace("");
        assertThat(place.getEntity(), IsNull.nullValue());
    }

    @Test
    public void getPlaceById_CorrectIdReturnNotNull() throws Exception {
        Response placesInDistrictByQueries = placeSP.getPlacesInDistrictByQueries("quang_ninh", "atm", "commented", "Bưu Điện Móng Cái", "");
        List<PlaceDetail> places = (List<PlaceDetail>) placesInDistrictByQueries.getEntity();

        ObjectId placeId = places.get(0).getId();

        PlaceDetail entity = (PlaceDetail) placeSP.getPlace(placeId.toHexString()).getEntity();

        assertThat(entity, IsNull.notNullValue());
        assertThat(entity.getId(), is(placeId));
    }

    @Test
    public void addPlace_NewPlaceAddedAndReturnId() throws Exception {
        Response placesInDistrictByQueries = placeSP.getPlacesInDistrictByQueries("quang_ninh", "atm", "commented", "Bưu Điện Móng Cái", "");
        List<PlaceDetail> places = (List<PlaceDetail>) placesInDistrictByQueries.getEntity();

        PlaceDetail placeDetail = places.get(0);
        ObjectId lastId = placeDetail.getId();
        placeDetail.setId(null);

        String newName = placeDetail.getName() + new Date().getTime();
        placeDetail.setName(newName);

        Response response = placeSP.addPlace(placeDetail);

        ObjectId placeId = (ObjectId) response.getEntity();
        assertThat(placeId, IsNull.notNullValue());
        assertThat(placeId, IsNot.not(lastId));

        placeDetail = (PlaceDetail) placeSP.getPlace(placeId.toHexString()).getEntity();
        assertThat(placeDetail, IsNull.notNullValue());
        assertThat(placeDetail.getName(), is(newName));
    }

    @Test
    public void getUpdateById_NewValuesAreSaved() throws Exception {
        Response placesInDistrictByQueries = placeSP.getPlacesInDistrictByQueries("quang_ninh", "atm", "commented", "Bưu Điện Móng Cái", "");
        List<PlaceDetail> places = (List<PlaceDetail>) placesInDistrictByQueries.getEntity();

        PlaceDetail placeDetail = places.get(0);
        ObjectId lastId = placeDetail.getId();
        placeDetail.setId(null);

        String newName = placeDetail.getName() + new Date().getTime();
        placeDetail.setName(newName);

        Response response = placeSP.addPlace(placeDetail);
        ObjectId placeId = (ObjectId) response.getEntity();
        assertThat(placeId, IsNull.notNullValue());
        assertThat(placeId, IsNot.not(lastId));

        placeDetail = (PlaceDetail) placeSP.getPlace(placeId.toHexString()).getEntity();
        assertThat(placeDetail, IsNull.notNullValue());
        assertThat(placeDetail.getName(), is(newName));

        String newName2 = placeDetail.getName() + new Date().getTime();
        placeDetail.setName(newName2);
        placeSP.updatePlace(placeId.toHexString(), placeDetail);

        placeDetail = (PlaceDetail) placeSP.getPlace(placeId.toHexString()).getEntity();
        assertThat(placeDetail.getName(), is(newName2));
    }

    public static double HaversineInKM(double lat1, double long1, double lat2, double long2) {
        final double _eQuatorialEarthRadius = 6378.1370D;
        final double _d2r = (Math.PI / 180D);
        double dlong = (long2 - long1) * _d2r;
        double dlat = (lat2 - lat1) * _d2r;
        double a = Math.pow(Math.sin(dlat / 2D), 2D) + Math.cos(lat1 * _d2r) * Math.cos(lat2 * _d2r)
                * Math.pow(Math.sin(dlong / 2D), 2D);
        double c = 2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));
        double d = _eQuatorialEarthRadius * c;

        return d;
    }

//    @Test
//    public void convertStringIdToObjectId2() throws Exception {
//        Jongo jongo = placeSP.getJongo();
//        MongoCollection collection = jongo.getCollection("placedetail");
//        MongoCursor<_PlaceDetail> mongoCursor = collection.find().as(_PlaceDetail.class);
//        int count = mongoCursor.count();
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        int index = 0;
//        List<String> ids = new ArrayList<String>();
//        for(_PlaceDetail oldPlace : mongoCursor){
//            if (index == count) return;
//            System.out.println(index++);
//
//            // add to ids
//            String id = oldPlace.getId();
//            ids.add(id);
//        }
//
//
//        // process every old ids
//        final int[] i = new int[1];
//        i[0] = 0;
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    try {
//                        Thread.sleep(20000);
//                        System.out.println(i[0]);
//                    } catch (InterruptedException e) {
//
//                    }
//                }
//            }
//        }).start();
//        for(String id : ids){
//            i[0]++;
//            _PlaceDetail oldPlace = collection.findOne("{_id : '" + id + "'}").as(_PlaceDetail.class);
//            if (oldPlace == null){
//                System.out.println(id);
//                continue;
//            }
//            // serialzie old
//            oldPlace.setId(null);
//            String oldJson = objectMapper.writeValueAsString(oldPlace);
//
//            // create new place
//            PlaceDetail newPlace = objectMapper.readValue(oldJson, PlaceDetail.class);
//            newPlace.setId(new ObjectId());
//
//            // create new object
//            collection.save(newPlace);
//
//            // delete old object
//            collection.remove("{_id : '"+id+"'}");
//
//            //            // check removed object
////            int count = collection.find("{_id : '" + id + "'}").as(_PlaceDetail.class).count();
////            assertThat(count, is(0));
////
////            // check inserted object
////            PlaceDetail inserted = collection.findOne(newPlace.getId()).as(PlaceDetail.class);
////            assertThat(inserted, IsNull.notNullValue());
//        }
//    }
}
